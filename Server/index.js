//import express và khởi chạy server node
//mailtest.sendmail1
import express from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import nodemailer from 'nodemailer';
import {updateUser, createUser} from '../User/user.js';
import {getListOrder, createOrder, updateOrder, deleteOrder} from '../Orders/order.js';
import {addItem, updateItem, getItem} from '../Items/item.js';

dotenv.config();

const app = express();
const port = process.env.PORT || 3200;


function Item(name, unitPrice) {
    this.name = name;
    this.unitPrice = unitPrice;
}

function User(username, password){
    this.username = username;
    this.password = password;
}

function Order(id, customerName = "", phoneNumber = "", email = "", item = null, quantity = 0, employeeName = "", dateCreated = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate(), status = "new"){
    this.id = id;
    this.customerName = customerName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.item = item;
    this.quantity = quantity;
    this.totalPrice = this.item.unitPrice * this.quantity;
    this.employeeName = employeeName.username;
    this.dateCreated = dateCreated;
    this.status = status;
}


const rootUser = new User('admin', '1234');
const hoiUser = new User('hoi', '1234');
const aUser = new User('A', '1234');
var currentUser = new User('', '');

let userList = [];
userList.push(rootUser);

const itemList = [];
itemList.push(new Item("Ly", 10000), new Item("Chén", 60000), new Item("Đũa", 2000));

var today = new Date();


var orderList = [
    new Order(1,"NguyenVanA", "0909999999", "nguyenvana@gmail.com", new Item("Ly", 10000), 2, rootUser,"2021-8-17"),
    new Order(2,"OngB", "02341231231", "OngB@gmail.com", new Item("Chén", 60000), 3, hoiUser, "2021-8-15"),
    new Order(3,"ChiC", "66666666666", "ChiC@gmail.com", new Item("Đũa", 2000), 4, aUser, "2021-8-19")
]


//Thêm 1 express middleware để có thể nhận dữ liệu json được submit lên từ phía client
app.use(express.json());

//Thêm một middleware để xác thực xem người dùng có đăng nhập hay chưa?
//Và token mà ngưỜi dùng gửi lên phía server có hợp lệ hay không?
function authenToken(req, res, next) {
    //Lấy ra token gửi lên từ phía client bằng cách
    const authorizationHeader = req.headers['authorization'];
    //authorization khi gửi lên từ client có dạng như sau:
    // Là 1 string gồm: 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        //console.log(err, data)
        //403 là người dùng k có quyền truy cập vào URL
        if(err) res.sendStatus(403);
        currentUser.username = data.username;
        next();
    })
}

export function checkRootUser(req, res, next){
    //Lấy ra token gửi lên từ phía client bằng cách
    const authorizationHeader = req.headers['authorization'];
    //authorization khi gửi lên từ client có dạng như sau:
    // Là 1 string gồm: 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        //console.log(err, data)
        //403 là người dùng k có quyền truy cập vào URL
        if(err) res.sendStatus(403);
        if(data.username !=='admin') res.sendStatus(403);
        next();
    })
}


function checkChangePassword(req, res, next){
    //Lấy ra token gửi lên từ phía client bằng cách
    const authorizationHeader = req.headers['authorization'];
    //authorization khi gửi lên từ client có dạng như sau:
    // Là 1 string gồm: 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        //console.log(err, data)
        //403 là người dùng k có quyền truy cập vào URL
        if(err) res.sendStatus(403);
        if(data.username !== req.body.username && data.username !== 'admin') res.sendStatus(403);
        currentUser.username = data.username;
        next();
    })
}

app.get('/books', authenToken, (req, res) => {
    res.json({status: 'Success', data: booksList})
})

app.post('/createUser', checkRootUser, (req, res) => {
    const data = req.body;
    createUser(data, userList);
    //main().catch(console.error);
    res.json({status: 'Success', data: userList})
});

app.post('/updateUser', checkRootUser, (req, res) => {
    const data = req.body;
    userList = userList.filter(e => e.username !== data.usernameUpdate);
    userList.push(updateUser(data));
    res.json({status: 'Success', data: userList})
});


app.post('/changePassword', checkChangePassword, (req, res) => {
    const data = req.body;
    userList = userList.filter(e => e.username !== data.username);
    userList.push(updateUser(data));
    res.json({status: 'Success', data: userList})
});

app.get('/getListOrder', authenToken, (req, res) => {
    getListOrder(req, res, orderList);
});


app.post('/createOrder', authenToken, (req, res) => {
    const data = req.body;
    let item = new Item(data.item.name, data.item.unitPrice);
    let order = new Order(orderList.length + 1,data.customerName, data.phoneNumber, data.email, item, data.quantity, currentUser);

    createOrder(req, res, orderList, order);
});

app.post('/updateOrder', authenToken, (req, res) => {
    updateOrder(req, res, orderList, currentUser, rootUser);
});

app.get('/deleteOrder', authenToken, (req, res) => {
    deleteOrder(req, res, currentUser, rootUser, orderList);
});

app.post('/addItem', checkRootUser, (req, res) => {
    const data = req.body;
    const item = new Item(data.item.name, data.item.unitPrice);
    addItem(req, res,item, itemList);
});

app.post('/updateItem', checkRootUser, (req, res) => {
    const data = req.body;
    const item = new Item(data.item.name, data.item.unitPrice);
    updateItem(req, res, item, itemList);
});

app.get('/getItem', authenToken, (req, res) => {
    getItem(req, res, itemList);
});



app.listen(port, ()=>{
    console.log(`listening on port: ${port}`);
});

