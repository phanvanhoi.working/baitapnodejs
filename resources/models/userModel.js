  
  import mongoose from 'mongoose';
  import bcryptjs from 'bcryptjs';

  const { Schema } = mongoose;

  const User = new Schema({
    username:  {type: String, unique: true, trim: true, required: true}, // String is shorthand for {type: String}
    password: String,
    email:  {type: String, unique: true, trim: true, required: true},
  });

  User.pre('save', function(next) {
    let user = this; 
    console.log(user)
    bcryptjs.hash(user.password, 10, function(err, hash) {
      if(err){
        return next(err);
      }else{
        user.password = hash;
        next();
      }
    })
  })

  User.pre('findOneAndUpdate', function(next) {
    let user = this; 
    
    bcryptjs.hash(user._update.password, 10, function(err, hash) {
      if(err){
        return next(err);
      }else{
        console.log("alo",user._update, hash);
        user._update.password = hash;
        next();
      }
    })
  })

  export default mongoose.model('User', User);

