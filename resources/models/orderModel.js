import mongoose from 'mongoose';
import Item from '../models/itemModel.js'

const { Schema } = mongoose;


const Order = new Schema({
    customerName: String,
    phoneNumber: String,
    email: String,
    item: {
        nameItem: String,
        unitPrice: Number
    },
    quantity: Number,
    totalPrice: { type: Number, default: (function() {
        return this.item.unitPrice * this.quantity;
    })},
    employeeName: String,
    dateCreated: { type: Date, default: Date.now },
    status: { type: String, default: "new" }
  });

  export default mongoose.model('Order', Order);