import mongoose from 'mongoose';
const { Schema } = mongoose;


const Item = new Schema({
    nameItem: String, // String is shorthand for {type: String}
    unitPrice: Number 
  });

  export default mongoose.model('Item', Item);
  