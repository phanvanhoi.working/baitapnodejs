import OrderRepository from '../Repositories/OrderRepository.js'
class OrderController {


    
    //[POST] /Item/addItem
    createOrder(req, res) {
        OrderRepository.createOrder(req);
    }

    //[GET] /Order/getOrder
    getListOrder(req, res){
        const param = req.query;
        if(!param){
            OrderRepository.getAllOrder().then(() => {
                if(!err) res.json({status: 'getAllOrder: Success', data: docs})
            })
        }else{

            switch(Object.keys(param)[0]) {
                case "customerName":
                    OrderRepository.getOrderByCustomerName().then((err, docs) =>{
                        if(!err) res.json({status: 'getOrderByCustomerName: Success', data: docs})
                    })
                    break;

                case "phoneNumber":
                    OrderRepository.getOrderByPhoneNumber().then((err, docs) => {
                        if(!err) res.json({status: 'getOrderByPhoneNumber: Success', data: docs})
                    })
                    break;

                case "employeeName":
                    OrderRepository.getOrderByEmployeeName().then((err, docs) => {
                        if(!err) res.json({status: 'getOrderByEmployeeName: Success', data: docs})
                    })
                    break;

                case "sortByDateCreate":
                    OrderRepository.getOrderSortByDateCreate().then((err, docs) => {
                        if(!err) res.json({status: 'getOrderSortByDateCreate: Success', data: docs})
                    })
                    break;
                    
                default:
                    OrderRepository.getAllOrder().then(() => {
                        if(!err) res.json({status: 'getAllOrder: Success', data: docs})
                    })
                }
        }
    }

    //[POST] /Order/updateStatus
    updateStatus(req, res){
        OrderRepository.updateStatus(req).then((err, docs) => {
            if (err) 
                console.log(err)
            else
                console.log("updateStatus: success")
        });
    }

    //[POST] /Order/updateOrder
    updateOrder(req, res){
        const user = getUser;
        const checkRootUser = user.username === "admin"? user.username : user.username;
        
        OrderRepository.getOrder(checkRootUser)
        .then((element) => {
            if(!element){
                res.sendStatus(403);
            }else{
                if(element.employeeName === user.username){
                    OrderRepository.updateOneOrder(req, user, element._id)
                    .then((err, docs) => {
                        if(!err) console.log("updateOrder: success")
                        else console.log("updateOrder: fail", err)
                    })
                }else res.sendStatus(403);
            }
        });
    }

    //[POST] /Order/deleteOrder
    deleteOrder(req, res){
        OrderRepository.deleteOrder(req).then((ok, deletedCount, n) => {
            console.log(ok, deletedCount, n)
        });
    }
}

export default new OrderController;

