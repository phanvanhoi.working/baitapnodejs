import ItemRepository from '../Repositories/ItemRepository.js'

class UserController {

    //[GET] /Item/getItem
    getItem(req, res){
        ItemRepository.getItem().then(() => {
            if(!err) res.json({status: 'Success', data: docs})
        })
    }

    //[POST] /Item/addItem
    addItem(req, res) {
        ItemRepository.addItem(req);
    }

    //[POST] /Item/updateItem
    updateItem(req, res) {
        ItemRepository.updateItem(req).then((err, docs) => {
            if (err){
                console.log(err)
            }
        })
    }
}

export default new UserController;

