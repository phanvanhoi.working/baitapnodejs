import UserRepository from '../Repositories/UserRepository.js'
import {transporter} from '../../config/mail/index.js'

class UserController {

    //[GET] /getUser
    getUsers(req, res){
        UserRepository.getUser().then((docs, err) => {
            if(!err)
                res.json({status: 'Success', data: docs})
        })
        

    }

    //[POST] /User/createUser
    createUser(req, res) {
        UserRepository.createUser(req).then((err, doc, num) => {
            if(!err){
                transporter.sendMail({
                    from: '"Hello" <nghianguyenhackathon@gmail.com>', // sender address
                    to: `${req.body.email}`, // list of receivers
                    subject: "Hello ✔", // Subject line
                    text: "Hello world?", // plain text body
                    html: "<b>Hello world?</b>", // html body
                });
            }
        })
    }

    //[POST] /User/updateUser
    updateUser(req, res ){
        UserRepository.updateUser(req).then((err, doc) => {
            if (!err){
                transporter.sendMail({
                    from: '"Hello" <nghianguyenhackathon@gmail.com>', // sender address
                    to: `${req.body.email}`, // list of receivers
                    subject: "Hello ✔", // Subject line
                    text: "Hello world?", // plain text body
                    html: "<b>Hello world?</b>", // html body
                });
            }
        });
    }

    //[POST] /User/updateUser
    changePassword(req, res ){
        UserRepository.changePassword(req, res).then((err,data) => {
            if(!err){
                console.log("Change password success:", data);
                res.sendStatus(200);
            }
        })
    }

 }

export default new UserController;

