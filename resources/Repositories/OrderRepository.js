import Order from '../models/orderModel.js'
import {getUser} from '../../Authen/userAuthen.js'
class OrderRepository {
    
    //[POST] /Item/addItem
    async createOrder(req) {
        const order = new Order(req.body)
        await order.save()
    }

    //[POST] /Item/addItem
    async getAllOrder() {
        await Order.find({});
    }

    async getOrderByCustomerName() {
        let param = req.query;
        await Order.find({customerName: param.customerName});
    }

    async getOrderByPhoneNumber() {
        let param = req.query;
        await Order.find({phoneNumber: param.phoneNumber});
    }

    async getOrderByEmployeeName() {
        let param = req.query;
        await Order.find({employeeName: param.employeeName});
    }

    async getOrderSortByDateCreate() {
        await Order.find().sort({dateCreated: 'descending'}).exec();
    }


    //[POST] /Order/updateStatus
    async updateStatus(req){
        await Order.updateOne({_id: req.body.id}, 
            {status: "completed"});
    }

    async getOrder(checkRootUser){
        return await Order.findOne({employeeName: checkRootUser});
    }

    async updateOneOrder(req, user, elementID){
        await Order.updateOne({_id: elementID}, 
            {
                customerName: req.body.customerName,
                phoneNumber: req.body.phoneNumber,
                email: req.body.email,
                item: {
                    nameItem: req.body.item.nameItem,
                    unitPrice: req.body.item.unitPrice,
                },
                quantity: req.body.quantity,
                employeeName: user.username,
            });
    }

    //[POST] /Order/deleteOrder
    async deleteOrder(req){
        await Order.deleteOne({_id: req.body.id});
    }
}

export default new OrderRepository;

