import {getUser} from '../../Authen/userAuthen.js'
import User from '../models/userModel.js'

class UserRepository  {
    //[GET] get
    async getUser() {
        let getUser = await User.find({});
        return getUser;
    }


    async createUser(req) {
        const user = new User(req.body)
        await user.save();
    }

    async updateUser(req){
        await User.updateOne({username: req.body.usernameUpdate}, 
                            {username: req.body.username, password: req.body.password});
    }

    async changePassword(req, res ){
        const user = getUser;
        if(req.body.password !== req.body.passwordAgain){
            res.sendStatus(403)
        }else{
            await User.findOneAndUpdate({username: user.username}, {password: req.body.password})
        }
    }
}

export default new UserRepository;
