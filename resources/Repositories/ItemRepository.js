import Item from '../models/itemModel.js'

class ItemRepository {

    //[GET] /Item/getItem
    async getItem(){
        await Item.find({});
    }

    //[POST] /Item/addItem
    async addItem(req) {
        const item = new Item(req.body);
        await item.save();
    }

    //[POST] /Item/updateItem
    async  updateItem(req) {
        await Item.updateOne({_id: req.body.id}, 
            {nameItem: req.body.nameItem, unitPrice: req.body.unitPrice});
    }
}

export default new ItemRepository;

