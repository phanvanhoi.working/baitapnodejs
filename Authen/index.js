//import express và khởi chạy server node

import express from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import User from '../resources/models/userModel.js'
import bcryptjs from 'bcryptjs';
import {connect} from '../config/db/index.js';

dotenv.config();
connect();

const app = express();
const port = 5500;

//Thêm 1 express middleware để có thể nhận dữ liệu json được submit lên từ phía client
app.use(express.json());

//Lưu các refreshToken lại
let refreshTokenList = [];

//Refresh token 
//Kiểm tra xem token gửi lên có hợp lệ hay k
//Ngoài việc trả về access token thì chúng ta cũng trả về 1 refresh token nữa
//Để khi jwt access token gần hết hạn thì dùng refresh token này gửi lên link /refreshToken
//Để tạo ra 1 token mới
app.post('/refreshToken', (req, res) => {
    //Kiểm tra xem refresh token gửi lên từ phía client có hợp lệ hay không?
    const refreshToken = req.body.token;

    //Unautowrite
    if(!refreshToken) res.sendStatus(401);

    //Không có quyền truy cập
    if(!refreshTokenList.includes(refreshToken)) res.sendStatus(403);

    jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, data) => {
        console.log(err, data);
        if(err) res.sendStatus(403);

        console.log("refreshToken dataUssername: ", data.user.username);
        //Refresh token hợp lệ, thì chúng ta tạo ra acess token mới
        //Ở đây payload k phải là data nữa
        const accessToken = jwt.sign({username: data.user.username}, process.env.ACCESS_TOKEN_SECRET);
        
        //Khi tạo được token thành công chúng ta sẽ gửi lại thông tin accessToken mới này về lại cho client
        res.json({accessToken});
    });
});


//Viết thêm 1 row login khi người dùng đăng nhập thành công thì chúng ta sẽ viết đoạn code để tạo ra
//một json web token mới và gửi ngược lại cho phía client để cho client có thể sử dụng token này
//cho các request tiếp theo
app.post('/login', (req, res) => {
    //Authorization
    //Giả sử client đăng nhập thành công 
    // giả sử client gửi lên thông tin là { userName: 'test'}, thì lúc này chúng ta sẽ lấy luôn thông tin này
    //Đưa vào payload của jwt 

    //Lấy ra thông tin người dùng submit lên
    const data = req.body;
    
    User.findOne({ username: data.username }, function (err, user) {
        if(err) res.sendStatus(403);
        console.log("LOGIN: ", user.username, req.body.password);
        if(bcryptjs.compareSync(req.body.password, user.password)){
            console.log("LOGIN1: ", user.username);
            //sign tham số thứ 1 là data tham số thứ 2 là secrit key, tham số thứ 3 là set thời hạn cho token
            const accessToken = jwt.sign({user}, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30s'});

            //Refresh token, k dùng expire để khi người dùng thoát thì mới kill token này
            const refreshToken = jwt.sign({user}, process.env.REFRESH_TOKEN_SECRET);
            refreshTokenList.push(refreshToken);
            //Bây giờ chúng ta sẽ gửi lại token này về lại cho client
            res.json({accessToken, refreshToken});
        }else{
            res.sendStatus(403);
        }
    });
        
});


//Kill refresh token khi người dùng log out 
app.post('/logout', (req, res) => {
    const refreshToken = req.body.token;
    refreshTokenList = refreshTokenList.filter(refresh => refresh !== refreshToken);
    res.sendStatus(200);
})

app.listen(port, ()=>{
    console.log(`listening on port: ${port}`);
});

