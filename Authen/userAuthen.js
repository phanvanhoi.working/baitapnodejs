import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

export var getUser = null;

export function checkRootUser(req, res, next){
    //Lấy ra token gửi lên từ phía client bằng cách
    const authorizationHeader = req.headers['authorization'];
    //authorization khi gửi lên từ client có dạng như sau:
    // Là 1 string gồm: 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);
    console.log("checkRootUser Token:",token);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        console.log(data);
        //403 là người dùng k có quyền truy cập vào URL
        if(err) res.sendStatus(err);
        if(data.username !=='admin'){
            res.sendStatus(403);
        }else{
            getUser = data;
            next();
        }
    })
}


//Thêm một middleware để xác thực xem người dùng có đăng nhập hay chưa?
//Và token mà ngưỜi dùng gửi lên phía server có hợp lệ hay không?
export function authenToken(req, res, next) {
        //Lấy ra token gửi lên từ phía client bằng cách
        const authorizationHeader = req.headers['authorization'];
        //authorization khi gửi lên từ client có dạng như sau:
        // Là 1 string gồm: 'Beaer [token]'
        const token = authorizationHeader.split(' ')[1];
        console.log("authenToken:",token);
        if(!token) res.sendStatus(401);
        
        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
            //console.log(err, data)
            //403 là người dùng k có quyền truy cập vào URL
            if(err) res.sendStatus(403);

            getUser = data;
            next();
        })
}

export function checkChangePassword(req, res, next){
    //Lấy ra token gửi lên từ phía client bằng cách
    const authorizationHeader = req.headers['authorization'];
    //authorization khi gửi lên từ client có dạng như sau:
    // Là 1 string gồm: 'Beaer [token]'
    const token = authorizationHeader.split(' ')[1];
    if(!token) res.sendStatus(401);
    
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        //console.log(err, data)
        //403 là người dùng k có quyền truy cập vào URL
        if(err) res.sendStatus(403);
        if(data.username !== req.body.username) res.sendStatus(403);

        getUser = data;
        next();
    })
}





