
export function getListOrder(req, res, orderList){
    const query = req.query;
    console.log(query);
    let resultList = JSON.parse(JSON.stringify(orderList));

    if(query.customerName){
        resultList = resultList.filter((e) => e.customerName === query.customerName)
    }
  
    if(query.phoneNumber)
    resultList = resultList.filter((e) => e.phoneNumber === query.phoneNumber)
    
    if(query.employeeName)
    resultList = resultList.filter((e) => e.employeeName === query.employeeName)

    if(query.sortByDateCreate !== undefined)
    resultList = resultList.sort((a, b) => Date.parse(a.dateCreated) - Date.parse(b.dateCreated))
  
    res.json({status: 'Success', data: resultList})
}

export function createOrder(req, res, orderList, order){
       
    orderList.push(order);
  
    res.json({status: 'Success', data: orderList})
}

export function updateOrder(req, res, orderList, currentUser, rootUser){
    const query = req.query;
    const data = req.body;
    const orderUpdated = orderList.find((e) => e.id == query.id);

    if(!((orderUpdated.employeeName === currentUser.username) && (orderUpdated.employeeName === rootUser.username))){
        res.json({status: 'You can not update this order', data: orderList})
    }
    else{
        orderList = orderList.filter((e) => e.id != query.id);

        orderUpdated.customerName = data.customerName;
        orderUpdated.phoneNumber = data.phoneNumber;
        orderUpdated.email = data.email;
        orderUpdated.item.name = data.item.name;
        orderUpdated.item.unitPrice = data.item.unitPrice;
        orderUpdated.quantity = data.quantity;
        orderUpdated.totalPrice = data.item.unitPrice * data.quantity;

        console.log(orderList, orderUpdated);

        orderList.push(orderUpdated);

        res.json({status: 'Success', data: orderList})
    }
}

export function deleteOrder(req, res, currentUser, rootUser, orderList){
    const query = req.query;

    if(currentUser.username !== rootUser.username){
        res.json({status: 'You can not delete this order', data: orderList});
    }
    else{
        console.log("AFTER:",orderList);
        orderList = orderList.filter((e) => e.id != query.id);
        console.log("BEFORE:",orderList);
        res.json({status: 'Success', data: orderList});
    }
}

