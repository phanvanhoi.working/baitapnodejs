

export function addItem(req, res, item, itemList){
    
    itemList.push(item);
    res.json({status: 'Success', data: itemList});
}

export function updateItem(req, res, item, itemList){
    
    itemList.splice(req.query.id -1, 1, item);

    res.json({status: 'Success', data: itemList});
}

export function getItem(req, res, itemList){
    res.json({status: 'Success', data: itemList});
}