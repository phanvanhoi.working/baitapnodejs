import express from 'express';
import routes from './router/index.js';
import {connect} from './config/db/index.js';
import User from './resources/models/userModel.js';
const app = express();

const port = process.env.PORT || 3002;

//Conect db
connect();

app.use(express.json()) 

//routes init
routes(app);
//### Client
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImhvaSIsImlhdCI6MTYyOTI4NTY3Mn0.SisKtLqKN43zJIC7AowbotmjJC6OmiRNyyRLKctbUq8

//### admin
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjI5MzQ1ODc1fQ.uOMLPdAyJeLTF4zjieqWQBdPmE0Oe4HAx0gk9yRIIKM


app.listen(port, ()=>{
    console.log(`listening on port: ${port}`);
});