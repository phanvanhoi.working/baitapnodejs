import express from 'express';
import itemController from '../resources/controllers/ItemController.js';
import { authenToken, checkRootUser, checkChangePassword } from '../Authen/userAuthen.js'

const router = express.Router();

router.get('/getItem', authenToken, itemController.getItem)
router.post('/addItem', checkRootUser, itemController.addItem)
router.post('/updateItem', checkRootUser, itemController.updateItem)

export default router;


