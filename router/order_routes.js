import express from 'express';
import orderController from '../resources/controllers/OrderController.js';
import { authenToken, checkRootUser, checkChangePassword } from '../Authen/userAuthen.js'

const router = express.Router();

router.get('/getListOrder', authenToken, orderController.getListOrder)

router.post('/createOrder', authenToken, orderController.createOrder)

router.post('/updateStatus', authenToken, orderController.updateStatus)

router.post('/updateOrder', authenToken, orderController.updateOrder)

router.post('/deleteOrder', checkRootUser, orderController.deleteOrder)

export default router;