import userRoutes from './user_routes.js'
import itemRoutes from './item_routes.js'
import orderRoutes from './order_routes.js'

function routes(app){

    app.use('/User', userRoutes);
    app.use('/Item', itemRoutes);
    app.use('/Order', orderRoutes);

}

export default routes;
