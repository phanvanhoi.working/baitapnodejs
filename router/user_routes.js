
import express from 'express';
const router = express.Router();
import userController from '../resources/controllers/UserController.js';
import { authenToken, checkRootUser, checkChangePassword } from '../Authen/userAuthen.js'

router.get('/getUser', authenToken, userController.getUsers)

router.post('/createUser', checkRootUser, userController.createUser)

router.post('/updateUser', checkRootUser, userController.updateUser)

router.post('/changePassword', authenToken, userController.changePassword)

export default router;





