import mongoose from 'mongoose';

export function connect() {
    
    //mongodb://127.0.0.1:27017/QuanLyBanHang
    //mongodb://mongo:27017/QuanLyBanHang
    mongoose.connect('mongodb://127.0.0.1:27017/QuanLyBanHang', { 
        useNewUrlParser: true,
        useUnifiedTopology: true 
    });
    mongoose.connection
        .once('open', () => console.log('Connected'))
        .on('error', (error) => {
                console.log("Error:", error);
        })

    
}


